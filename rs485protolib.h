#ifndef RS485PROTOLIB_H_
#define RS485PROTOLIB_H_

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

#define MAX_DATABYTES 8

#define RS485CommandRegisterMaxSize 4

#define RS485CommandAcceptDirect 1
#define RS485CommandAcceptBroadcast 2

typedef enum
{
  NEED_INIT = 0, // Component not initialised yet.
  // Slave
  AWAIT_REQUEST_FETCH_ADDRESS, // First "real" state, RS485 slave waiting for request from master.
  AWAIT_REQUEST_FETCH_COMMAND, // After the address we expect the parameter bytes. First one is "command".
  AWAIT_REQUEST_FETCH_NBPARAMS, // Now read the number of parameters (might be 0).
  AWAIT_REQUEST_FETCH_PARAMS, // Finally read as many parameter bytes as signaled by the master.
  AWAIT_REQUEST_FETCH_CRC, // Read the CRC8 sum
  VALIDITY_CHECK, // Check if CRC sum valid
  PROCESSING_REQUEST, // Now the request has to be processed.

  AWAIT_RESPONSE_PUSH_ADDRESS, // Request from master received. Master expects response.
  AWAIT_RESPONSE_PUSH_COMMAND, // Push the command, to which the hardware answers
  AWAIT_RESPONSE_PUSH_NBDATA, // Push number of data bytes sent
  AWAIT_RESPONSE_PUSH_DATA, // Push actual data
  AWAIT_RESPONSE_PUSH_CRC, // Push CRC8 sum
  FINISH_TRANSMISSION, // Finish transmission, return to receiving
} RS485State;

typedef struct
{
  uint8_t address;
  uint8_t command;
  uint8_t numberOfParameters;
  uint8_t parameter[MAX_DATABYTES];
  uint8_t crc8;
  bool respond;
} RS485RequestMessage;

typedef struct
{
  uint8_t address;
  uint8_t command;
  uint8_t numberOfReturnValues;
  uint8_t returnValue[MAX_DATABYTES];
  uint8_t crc8;
} RS485ResponseMessage;

typedef enum
{
  OK = 0, // Everything went fine.
  ZERO_ADDRESS_NOT_ALLOWED, // 0 is the broadcast address, particular slave must have address >=1 && <=127.
  ADDRESS_TO_BIG, // Address must be < 128
  NO_RESPONSE_EXPECTED, // Tried to send message to master without being asked to do so.
  INVALID_STATE_WHEN_RECEIVING, // State machine confused somehow when reading.
  NO_REQUEST_AVAILABLE, // Tried to read request but no message was received.
  NO_RESPONSE_AVIABLE, // No response aviable
  INVALID_STATE_WHEN_SENDING, // State machine confused somehow when sending.
  REQUEST_DROPPED, // The message has not been consumed yet, so subsequent messages are dropped.
  NULL_REQUEST_BUFFER, // User program has to provide memory for retrieving the request.
  STATE_MACHINE_RESET, // Unexpected address was received.
  CRC_MISMATCH, // Wrong CRC8
  //
  COMMAND_NOT_FOUND,
  COMMAND_WRONG_TYPE
} RS485Status;

typedef void (*RS485CommandHandler)(const RS485RequestMessage*, RS485ResponseMessage*);

typedef struct
{
  uint8_t id;
  uint8_t type;
  RS485CommandHandler handler;
} RS485Command;

RS485Status initializeRS485slave(const uint8_t p_myAddress);

bool rs485MessageAvailable();

RS485Status consumeRS485Request();
RS485Status respondViaRS485(const RS485ResponseMessage* p_response);

RS485Status recentRS485Status();

uint8_t computeRequestCrc8(const RS485RequestMessage* p_msg);
uint8_t computeResponseCrc8(const RS485ResponseMessage* p_msg);
uint8_t genericCrc8(const uint8_t* data_pointer, const uint8_t number_of_bytes);

void stateTransition(void);
RS485Status status(const RS485Status status);

void registerRS485Command(const uint8_t id, const uint8_t type, const RS485CommandHandler handler);
void unregisterRS485Command(const uint8_t id);
RS485Status executeRS485Command(const RS485RequestMessage*, RS485ResponseMessage*);

void addResponseValue(RS485ResponseMessage* rsp, uint8_t val);

#endif /* RS485PROTOLIB_H_ */
