#ifndef RS485HWLIB_H_
#define RS485HWLIB_H_

#define BAUD 9600
#include <util/setbaud.h>
#define RS485_SWITCHING_DELAY 5

#include "rs485protolib.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdbool.h>

static inline void HW_ISRoff(void)
{
  cli();
  return;
}

static inline void HW_ISRon(void)
{
  sei();
  return;
}

static inline void HW_TxISR_On(void)
{
  //clear Tx interrupt
  UCSRA |= (1 << TXC);
  //enable Tx interrupt
  UCSRB |= (1 << TXCIE);
  return;
}

static inline void HW_TxISR_Off(void)
{
  UCSRB &= ~(1 << TXCIE);
  return;
}

static inline void HW_UARTinit(void)
{
  //---- Port D is used to select RS485 transceiver direction ----------------
  // Set pin PD2 of PORTD to output.
  DDRD |= _BV(DDD2);
  PORTD &= ~(1 << PD2); // Set RS485 transceiver to receive.

  UBRRH = UBRRH_VALUE;
  UBRRL = UBRRL_VALUE;

  // This statement is translated into a read-modify-write instruction.
  // This might cause problems according to the ATmega8 data sheet (section
  // "Muti-processor Communication Mode").
  // It is stated that using CLI or SBI on MCPM might accidentally clear
  // the TXC flag since it shares the same I/O location as MPCM.
  //
  // In this case we want to clear TXC anyway. But in other places in this
  // code this might cause trouble and need to be considered. Use the compiler
  // switch "-save-temps" to produce an assembler file. In that file you can
  // verify that nor CLI/SBI has been generated.
  UCSRA = (1 << MPCM); // Multiprocessor mode, bit 9 determines address byte.
  #if USE_2X
  UCSRA |= (1 << U2X);
  #else
  UCSRA &= ~(1 << U2X);
  #endif
  UCSRB = (1 << TXEN) | (1 << RXEN) | (1 << RXCIE)  | (1 << UCSZ2); // UART TX and RX on = send and receive, receive and transmit triggering interrupts
  UCSRB &= ~(1 << TXB8); // Clear 9th data bit. We only return data no address.
  UCSRC = (1 << URSEL) | (1 << UCSZ1) | (1 << UCSZ0); // Asynchronus 9N1
}

static inline void
HW_DELAY(void)
{
  _delay_ms(RS485_SWITCHING_DELAY);
  return;
}

static inline void
HW_RTSset(void)
{
  // Set RS485 transceiver to send.
  PORTD |= (1 << PD2);
  return;
}

static inline void
HW_RTSclear(void)
{
  PORTD &= ~(1 << PD2);
  return;
}

static inline bool
HW_UART_TXbusy(void)
{
  return !(UCSRA & (1 << UDRE)); // is hardware send buffer ready.
}

static inline void
HW_UARTsend(uint8_t data)
{
  while (HW_UART_TXbusy())
    ;
  UDR = data;
}

static inline bool
HW_UART_RXbusy(void)
{
  return !(UCSRA & (1 << RXC)); // is hardware receive buffer ready?
}

static inline uint8_t
HW_UARTget(void)
{
  while (HW_UART_RXbusy())
    ;
  return UDR;
}

// INTENDED USE: during RECEIVING ONLY
static inline void
HW_QuitMPMode()
{
  UCSRA &= ~(1 << MPCM); // quit Multiprocessor mode, we might clear TXC, so use this ONLY when receiving
}

static inline void
HW_EnterMPMode()
{
  UCSRA |= (1 << MPCM);
}

static inline bool
HW_UARTaddress(void)
{
  return (bool) (UCSRB & (1 << RXB8));
}


ISR(USART_RXC_vect)
{
  stateTransition();
}

ISR(USART_TXC_vect)
{
  stateTransition();
}

#endif /* RS485HWLIB_H_ */
