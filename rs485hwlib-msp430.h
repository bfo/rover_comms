#ifndef RS485HWLIB_H_
#define RS485HWLIB_H_

#define RS485_SWITCHING_DELAY 5
#define RS485CommandRegisterMaxSize 4

#include <msp430g2553.h>
#include <legacymsp430.h>
#include <stdbool.h>

void stateTransition(void);

static inline void HW_ISRoff(void)
{
  _DINT();
  return;
}

static inline void HW_ISRon(void)
{
  _EINT();
  return;
}

static inline void HW_UARTinit(void)
{
  //---- Select and initialize pin to control RS485 transceiver direction ----------------
  P1DIR |= BIT0;
  P1OUT &= ~BIT0; // Set RS485 transceiver to receive.

  //assign P1.1 and P1.2 to be used as Rx and Tx
  P1SEL |= BIT1 + BIT2;
  P1SEL2 |= BIT1 + BIT2;

  //no parity,
  //LSB first,
  //8-bit data,
  //one stop bit,
  //address-bit multiprocessor mode,
  //async mode
  UCA0CTL0 |= UCMODE_0;

  //select clock source - SMCLK,
  //erroneous characters rejected and UCAxRXIFG is not set,
  UCA0CTL1 |= UCSSEL_2;

  //example prescaler settings:
  //minimal error rate without external oscillator
  //BRCLK == SMCLK == 1MHz
  //baud == 9600
  //UCBRx == 104
  //UCBRSx == 1
  //UCBRFx == 0
  UCA0BR0 = 104;
  UCA0BR1 = 0;
  UCA0MCTL = UCBRS_1;

  //release USCI for operation
  UCA0CTL1 &= ~UCSWRST;

  //enable Rx interrupt
  IE2 |= UCA0RXIE;
}

static inline void HW_TxISR_On(void)
{
  //clear Tx interrupt
  IFG2 &= ~(UCA0TXIFG);
  //enable Tx interrupt
  IE2 |= UCA0TXIE;
  return;
}

static inline void HW_TxISR_Off(void)
{
    IE2 &= ~(UCA0TXIE);
  return;
}

static inline void HW_DELAY(void)
{
  __delay_cycles(RS485_SWITCHING_DELAY);
  return;
}

static inline void HW_RTSset(void)
{
  // Set RS485 transceiver to send.
  P1OUT |= BIT0;
  return;
}

static inline void HW_RTSclear(void)
{
  P1OUT &= ~BIT0; // Set RS485 transceiver to receive.
  return;
}

//NOTE: since msp430 behaves little different than AVR, there is no way to distinguish that tx buffer is ready to accept new data or that tx_isr is taking (or should take) place, as this information shares one data bit
//this required some minor changes in protolib code (enabling and disabling of tx interrupt)
//what's more, TXbusy and RXbusy functions below BOTH return information if a transmit or receive operation is in progress. I haven't found easier and more informative way of implementing that other than simply returning UCBUSY bit in USCI_Ax Status Register
//shit happens
//More explanation: 'normal' way of checking if tx buffer is ready is to read (IFG2 & UCA0TXIFG). Since UCA0TXIFG is used by ISR, it must be reset to prevent premature ISR request. And since we reset that bit we loose information about tx buffer.

static inline bool HW_UART_TXbusy(void)
{
  return (UCA0STAT & UCBUSY);
}

static inline bool HW_UART_RXbusy(void)
{
  return (UCA0STAT & UCBUSY);
}

static inline void HW_UARTsend(uint8_t data)
{
  while (HW_UART_TXbusy());
  UCA0TXBUF = data;
}

static inline uint8_t HW_UARTget(void)
{
  while (HW_UART_RXbusy());
  return UCA0RXBUF;
}

static inline void HW_QuitMPMode()
{
  UCA0CTL1 &= ~(UCDORM); // quit Multiprocessor mode
}

static inline void HW_EnterMPMode()
{
  UCA0CTL1 |= UCDORM; // enter Multiprocessor mode
}

static inline bool HW_UARTaddress(void)
{
  return (bool) (UCA0STAT & UCADDR);
}

interrupt(USCIAB0RX_VECTOR) USCIAB0RX_ISR(void){
//ISR(USART_RXC_vect)
  stateTransition();
}

interrupt(USCIAB0TX_VECTOR) USCIAB0TX_ISR(void){
//ISR(USART_TXC_vect)
  stateTransition();
}

#endif /* RS485HWLIB_H_ */

