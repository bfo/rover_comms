#include "rs485protolib.h"

#include <stddef.h>
#include <stdint.h>
#include <util/delay.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>

#define RS485_SLAVE_ADDRESS 1
#define RS485_MASTER_ADDRESS 0

void
initialize_hardware(void)
{
  cli();
  DDRB |= _BV(PB0) | _BV(PB1) | _BV(PB2);
  PORTB = 0x00;
  sei();
}

// Here example of command handler
void
blink(const RS485RequestMessage* rq, RS485ResponseMessage* rsp)
{
  static int c = 0;
  PORTB = c++;
  if (c > 7)
    c = 0;
}
void
blinkAndResponse(const RS485RequestMessage* rq, RS485ResponseMessage* rsp)
{
  static uint8_t c = 0;
  PORTB = c++;
  if (c > 7)
    c = 0;
  addResponseValue(rsp, c);
}
void
echo(const RS485RequestMessage* rq, RS485ResponseMessage* rsp)
{

  addResponseValue(rsp, rq->parameter[0]);
}

int
main(void)
{
  initialize_hardware();

  // Initialize RS485 for slave
  initializeRS485slave(RS485_SLAVE_ADDRESS);

  // Here example of registering command of ID=1, which can be invoked by both
  // "broadcast" and "direct" request and calls function blink to do stuff.
  registerRS485Command(1,
      RS485CommandAcceptBroadcast | RS485CommandAcceptDirect, blink);
  registerRS485Command(2, RS485CommandAcceptDirect, blinkAndResponse);
  registerRS485Command(3, RS485CommandAcceptDirect, echo);

  while (true)
    {
      // If any request is aviable try to process
      if (rs485MessageAvailable())
        {
          consumeRS485Request();
        }

      // If something went very very wrong simply do nothing
      if (OK != recentRS485Status())
        {
          // Scream!
        }

      // Do your stuff here . . .
    }
}
