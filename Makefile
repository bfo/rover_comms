#USAGE INSTRUCTION:
#$ make <what> ARCH=[MSP or AVR]
#example:
#$ make clean ARCH=AVR
#$ make bin ARCH=MSP
#$ make help ARCH=AVR

#always specify ARCH variable!

#==========================================================================
#REMEMBER TO CLEAN BEFORE BUILDING FOR DIFFERENT ARCH OR LINKER WILL FAIL
#==========================================================================

#name of target file:
TARGET   = main

ifeq ($(ARCH),AVR) 
CC       = avr-gcc
OBJCOPY	 = avr-objcopy
SIZE	 = avr-size -t
MCU      = atmega8
EXTRA_CFLAGS = -D AVR -D F_CPU=8000000UL
SOURCES  = main-slave-avr.c rs485protolib.c
FLASH_DEVICE_CMD = avrdude -c usbasp -p m8 -U flash:w:$(TARGET).hex
endif

ifeq ($(ARCH),MSP) 
CC       = msp430-gcc
OBJCOPY	 = msp430-objcopy
SIZE	 = msp430-size -t
MCU      = msp430g2553
EXTRA_CFLAGS = -D MSP
SOURCES  = main-slave-msp430.c rs485protolib.c
MSPDEBUG     = mspdebug
MSPTYPE      = rf2500
FLASH_DEVICE_CMD = $(MSPDEBUG) $(MSPTYPE) "prog $(TARGET).elf"
endif

#so far unneeded variables:
SOURCESA =
INCLUDES = 
LIBS     =

# Flags and command lines
CFLAGS   = -mmcu=$(MCU) -g -Os $(EXTRA_CFLAGS) -Wall -Wunused $(INCLUDES)
ASFLAGS  = -mmcu=$(MCU) -x assembler-with-cpp -Wa,-gstabs
LDFLAGS  = -mmcu=$(MCU) -Wl,-Map=$(TARGET).map

# Object files and listings
OBJS     = $(SOURCES:.c=.o) $(SOURCESA:.asm=.o)
LSTS     = $(SOURCES:.c=.lst)

# All, to clean, build the binary and program it
all: bin prog

# Binary, depends on the object files
bin: $(TARGET).elf $(TARGET).hex
$(TARGET).elf: $(OBJS)
ifeq ($(ARCH),)
	@echo "ERROR: ARCH not defined."
	@exit 1
endif
	$(CC) $(LDFLAGS) -o $(TARGET).elf $(OBJS) $(LIBS)
	$(SIZE) $(TARGET).elf

listing: $(LSTS)

# Compile the object files
%.o: %.c
ifeq ($(ARCH),)
	@echo "ERROR: ARCH not defined."
	@exit 1
endif
	$(CC) -c $(CFLAGS) -o $@ $<

%.o: %.asm
ifeq ($(ARCH),)
	@echo "ERROR: ARCH not defined."
	@exit 1
endif
	$(CC) -c $(ASFLAGS) -o $@ $<

# Create hex files
%.hex: %.elf
	$(OBJCOPY) -O ihex $< $@

# rule for making assembler source listing, to see the code
%.lst: %.c
	$(CC) -c $(CFLAGS) -Wa,-anlhd $< > $@

# Clean
clean:
ifeq ($(ARCH),)
	@echo "ERROR: ARCH not defined."
	@exit 1
endif
	rm -fr $(TARGET).hex $(TARGET).elf $(TARGET).map $(OBJS) $(LSTS)

prog: $(TARGET).elf
	$(FLASH_DEVICE_CMD)

package: clean
	@echo "Packaging source..."
	@bash -c "test -e $(TARGET)-src.tgz && rm -rf $(TARGET)-src.tgz; exit 0"
	@echo "Copying files..."
	@mkdir /tmp/$(TARGET)-src
	@cp * /tmp/$(TARGET)-src
	@echo "Creating tarball..."
	@cd /tmp && tar cfz $(TARGET)-src.tgz $(TARGET)-src
	@mv /tmp/$(TARGET)-src.tgz .
	@echo "Cleaning up..."
	@rm -rf /tmp/$(TARGET)-src

help:
	@echo "Target binary:"
	@echo "  $(TARGET)"
	@echo
	@echo "Source files:"
	@echo "  $(SOURCES) $(SOURCESASM)"
	@echo 
	@echo "Supported rules:"
	@echo "  all       Build and program the device"
	@echo "  bin       Only build binary"
	@echo "  prog      Only program device with binary"
	@echo "  clean     Clean the build environment"
	@echo "  listing   Assembly source listings"
	@echo "  package   Create tarball package"
	
