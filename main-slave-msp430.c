#include <msp430g2553.h>
#include <legacymsp430.h>
#include <stdbool.h>

#include "rs485protolib.h"

#define RS485_SLAVE_ADDRESS 1
#define RS485_MASTER_ADDRESS 0

void _INIT(void) {
	//setup WDT
	//WDT off
	WDTCTL = WDTPW + WDTHOLD;

	//ACLK - set to LFXT1 = VLO
	BCSCTL3 |= LFXT1S_2;

	//clear OSCFault flag
	//wait for clock fault system to react

	do {
		IFG1 &= ~OFIFG; // Clear OSCFault flag
	} while (BCSCTL3 & LFXT1OF);

	//DCO 1MHz
	if (CALDCO_1MHZ == 0xFF || CALBC1_1MHZ == 0xFF) {
		//calibration data was erased! we can't proceed
		while (true);
	} else {
		BCSCTL1 = CALBC1_1MHZ;
		DCOCTL = CALDCO_1MHZ;
	}

	//MCLK - default DCO/1

	//SMCLK - default DCO/1

	//setup GPIO
	P1DIR |= BIT6;
	P1OUT |= (BIT6);
}
void blink(const RS485RequestMessage* rq, RS485ResponseMessage* rsp){
	P1OUT ^= BIT6;
}

void blinkAndResponse(const RS485RequestMessage* rq, RS485ResponseMessage* rsp){
	P1OUT ^= BIT6;
	addResponseValue(rsp, (P1OUT & BIT6));
}

void main(void) {
	_INIT();

	registerRS485Command(1, RS485CommandAcceptBroadcast | RS485CommandAcceptDirect, blink);
	registerRS485Command(2, RS485CommandAcceptDirect, blinkAndResponse);

	initializeRS485slave(RS485_SLAVE_ADDRESS);
	while (true)
	    {
	      // If any request is aviable try to process
	      if (rs485MessageAvailable())
	        {
	          consumeRS485Request();
	        }

	      // Do your stuff here . . .
	    }
}

